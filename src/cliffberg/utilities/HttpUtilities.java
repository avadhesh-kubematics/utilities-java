package cliffberg.utilities;

import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class HttpUtilities {
	public static String getHttpResponseAsString(URL url) throws Exception {

		HttpURLConnection httpConn = null;
		try {
			httpConn = getConnection(url);
			Object response = httpConn.getContent();

			if (response == null) {
				return null;

			} else if (response instanceof String) {
				return (String)response;

			} else if (response instanceof InputStream) {
				InputStream inst = (InputStream)response;
				String str = "";
				BufferedReader br = new BufferedReader(new InputStreamReader(inst));
				for (;;) {
					String line = br.readLine();
					if (line == null) break;
					str += line;
				}
				return str;

			} else throw new Exception(
				"Response is not a string or HttpURLConnection: it is a " + response.getClass().getName());

		} catch (RuntimeException rex) {
			System.err.println("A RuntimeException response from getConnection: " + rex.getMessage());
			rex.printStackTrace(System.err);
			throw rex;
		} catch (Exception ex) {
			System.err.println("An Exception response from getConnection: " + ex.getMessage());
			throw ex;
		} finally {
			if (httpConn != null) try { httpConn.disconnect(); } catch (Exception ex3) {}
		}
	}

	public static HttpURLConnection getConnection(URL url) throws Exception {

		URLConnection conn = url.openConnection();
		if (! (conn instanceof HttpURLConnection)) throw new Exception(
				"Object returned is not a HttpURLConnection: it is a " + conn.getClass().getName());
		HttpURLConnection hconn = (HttpURLConnection)conn;
		hconn.connect();

		/* Check for an HTTP error code. */
		int respCode = hconn.getResponseCode();
		String respMsg = hconn.getResponseMessage();
		if (respCode >= 500) {
			throw new RuntimeException(respMsg);
		} else if (respCode >= 400) {
			throw new Exception(respMsg);
		} else if (respCode >= 300) {
			throw new Exception(respMsg);
		}

		return hconn;
	}
}
